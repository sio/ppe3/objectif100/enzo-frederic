#ifndef GENRE_H
#define GENRE_H
#include <string>

/**
 * \class Genre
 * \brief Definition of the Genre class
 */

class Genre
{
 public:

  /**
   * \brief Default constructor of Genre
   */
  Genre();

  /**
   * \brief Constructor with parameters
   * \param[in] _id the identification number of a genre
   * \param[in] _type the type of a genre
   */
  Genre(unsigned int _id, std::string _type);

  /**
   * \brief Lets you know the id of a genre
   * \return unsigned int representing the id of a genre
   */
  unsigned int get_id();

  /**
   * \brief Lets you know the type of a genre
   * \return std::string representing the type of a genre
   */
  std::string get_type();


 private:
  unsigned int id;///< Attribute defining the id of the genre
  std::string type;///< Attribute determining the type of a genre
};
#endif
