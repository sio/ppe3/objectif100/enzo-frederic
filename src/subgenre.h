#ifndef SUBGENRE_H
#define SUBGENRE_H
#include <string>

/**
* \class SubGenre
* \brief Definition of the SubGenre class
*/
class SubGenre
{
 public:

  /**
   * \brief Default constructor of SubGenre
   */
  SubGenre();

  /**
  * \brief Constructor with parameters
  * \param[in] _id the identification number of a subgenre
  * \param[in] _sub_type the sub type of a subgenre
  */
  SubGenre(unsigned int _id, std::string _sub_type);

  /**
  * \brief Lets you know the id of a subgenre
  * \return unsigned int representing the id of a subgenre
  */
  unsigned int get_id();

  /**
  * \brief Lets you know the type of a subgenre
  * \return std::string representing the sub type of a subgenre
  */
  std::string get_sub_type();

 private:
  unsigned int id; ///< Attribute defining the id of the subgenre
  std::string sub_type; ///< Attribute determining the sub type of the subgenre
};
#endif
