#ifndef TRACK_H
#define TRACK_H
#include <string>
#include <vector>
#include <chrono>
#include "artist.h"
#include "album.h"
#include "polyphony.h"
#include "format.h"
#include "subgenre.h"
#include "genre.h"

/**
 * \class Track
 * \brief Definition of the Track class
 */
class Track
{
 public:

  /**
    * \brief Default constructor of Track
    */
  Track();

  /**
    * \brief Constructor with parameters
    * \param[in] _id the identification number of a track
    * \param[in] _duration the duration of a track
    * \param[in] _name the name of a track
    * \param[in] _path the path of a track
    * \param[in] _artists artists of a track
    * \param[in] _albums albums of a track
    * \param[in] _a_polyphony the polyphony of a track
    * \param[in] _a_format the format of a track
    * \param[in] _a_subgenre the subgenre of a track
    * \param[in] _a_genre the genre of a track
    */
  Track(unsigned int _id, std::chrono::duration<int> _duration, std::string _name, std::string _path, std::vector<Artist> _artists, std::vector<Album> _albums, Polyphony _a_polyphony, Format _a_format, SubGenre _a_subgenre, Genre _a_genre);

  /**
   * \brief Lets you know the id of a track
   * \return unsigned int representing the id of a track
   */
  unsigned int get_id();

  /**
   * \brief Lets you know the duration of a track
   * \return std::chrono::duration<int> representing the duration of a track
   */
  std::chrono::duration<int> get_duration();

  /**
   * \brief Lets you know the name of a track
   * \return std::string representing the name of a track
   */
  std::string get_name();

  /**
   * \brief Lets you know the path of a track
   * \return std::string representing the path of a track
   */
  std::string get_path();

  /**
   * \brief Lets you know artist of a track
   * \return std::vector<Artist> representing artists of a track
   */
  std::vector<Artist> get_artists();

  /**
   * \brief Lets you know albums of a track
   * \return std::vector<Album> representing albums of a track
   */
  std::vector<Album> get_albums();

  /**
   * \brief Lets you know the polyphony of a track
   * \return Polyphony representing the polyphony of a track
   */
  Polyphony get_polyphony();

  /**
   * \brief Lets you know the format of a track
   * \return Format representing the format of a track
   */
  Format get_format();

  /**
   * \brief Lets you know the subgenre of a track
   * \return SubGenre representing the subgenre of a track
   */
  SubGenre get_subgenre();

  /**
   * \brief Lets you know the genre of a track
   * \return Genre representing the genre of a track
   */
  Genre get_genre();

 private:
  unsigned int id;///< Attribute defining the id of the track
  std::chrono::duration<int> duration;///< Attribute determining the duration of a track
  std::string name;///< Attribute determining the name of a track
  std::string path;///< Attribute determining the path of a track
  std::vector<Artist> artists;///< Attribute determining artists of a track
  std::vector<Album> albums;///< Attribute determining albums of a track
  Polyphony a_polyphony;///< Attribute determining the polyphony of a track
  Format a_format;///< Attribute determining the format of a track
  SubGenre a_subgenre;///< Attribute determining the subgenre of a track
  Genre a_genre;///< Attribute determining the genre of a track
};
#endif
