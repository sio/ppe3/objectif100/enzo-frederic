#ifndef CLI_H
#define CLI_H
#include <string>
#include <gflags/gflags.h>
#include <gflags/gflags_declare.h>

DECLARE_uint64(duration);
DECLARE_string(genre);
DECLARE_string(type);
DECLARE_string(name);
DECLARE_string(playlist);
DECLARE_string(subgenre);
DECLARE_string(artist);
DECLARE_string(album);


#endif
