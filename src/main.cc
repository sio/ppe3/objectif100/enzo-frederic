#include <fstream>
#include <iostream>
#include <XspfWriter.h>
#include <XspfTrack.h>
#include <XspfIndentFormatter.h>
#include "track.h"
#include "playlist.h"
#include "cli.h"

int main(int argc, char *argv[])
{
  int code_retour = 0;
  gflags::ParseCommandLineFlags(&argc, &argv, true);
  std::string genre = FLAGS_genre;
  std::string album = FLAGS_album;
  std::string artist = FLAGS_artist;
  std::string name = FLAGS_name ;
  unsigned int duree_demandee = FLAGS_duration;
  std::string requete = "";
  Playlist *testPlaylist = new Playlist();
  requete = "SET SCHEMA 'radio_libre'; select * from \"morceau\" inner join \"artiste_morceau\" on \"morceau\".id = \"artiste_morceau\".id_morceau inner join \"artiste\" on \"artiste_morceau\".id_artiste = \"artiste\".id inner join \"album_morceau\" on \"morceau\".id = \"album_morceau\".id_morceau inner join \"album\" on \"album_morceau\".id_album = \"album\".id inner join \"genre\" on \"morceau\".fk_genre = \"genre\".id where \"genre\".type ~ '" + genre + "' AND \"album\".nom  ~ '" + album + "' AND \"artiste\".nom ~ '" + artist + "' AND \"morceau\".nom ~ '" + name + "';";
  testPlaylist->writeXSPF(requete, duree_demandee);
  testPlaylist->writeM3U(requete, duree_demandee);
  return code_retour;
}
