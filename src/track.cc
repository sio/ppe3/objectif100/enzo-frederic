#include <iostream>
#include "track.h"


Track::Track() :
  id(0),
  duration(0),
  name(""),
  path(""),
  artists(),
  albums(),
  a_polyphony(),
  a_format(),
  a_subgenre(),
  a_genre()
{}

Track::Track(unsigned int _id, std::chrono::duration<int> _duration, std::string _name, std::string _path, std::vector<Artist> _artists, std::vector<Album> _albums, Polyphony _a_polyphony, Format _a_format, SubGenre _a_subgenre, Genre _a_genre) :

  id(_id),
  duration(_duration),
  name(_name),
  path(_path),
  artists(_artists),
  albums(_albums),
  a_polyphony(_a_polyphony),
  a_format(_a_format),
  a_subgenre(_a_subgenre),
  a_genre(_a_genre)

{}

unsigned int Track::get_id()
{
  return id;
}

std::chrono::duration<int> Track::get_duration()
{
  return duration;
}

std::string Track::get_name()
{
  return name;
}

std::string Track::get_path()
{
  return path;
}

std::vector<Artist> Track::get_artists()
{
  return artists;
}

std::vector<Album> Track::get_albums()
{
  return albums;
}

Polyphony Track::get_polyphony()
{
  return a_polyphony;
}

Format Track::get_format()
{
  return a_format;
}

SubGenre Track::get_subgenre()
{
  return a_subgenre;
}

Genre Track::get_genre()
{
  return a_genre;
}
