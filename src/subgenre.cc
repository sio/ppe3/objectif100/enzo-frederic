#include <iostream>
#include "subgenre.h"


SubGenre::SubGenre() :
  id(0),
  sub_type("")
{}

SubGenre::SubGenre(unsigned int _id,
                   std::string _sub_type) :
  id(_id),
  sub_type(_sub_type)
{}

unsigned int SubGenre::get_id()
{
  return id;
}

std::string SubGenre::get_sub_type()
{
  return sub_type;
}

