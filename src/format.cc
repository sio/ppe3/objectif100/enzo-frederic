#include <iostream>
#include "format.h"

Format::Format() :
  id(0),
  titule("")
{}


Format::Format(unsigned int _id,
               std::string _titule) :

  id(_id),
  titule(_titule)
{}

unsigned int Format::get_id()
{
  return id;
}

std::string Format::get_titule()
{
  return titule;
}

void Format::set_titule(std::string _titule)
{
  titule = _titule;
}
